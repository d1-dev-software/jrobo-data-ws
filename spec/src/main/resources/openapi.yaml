openapi: 3.0.0
info:
  title: jroboDataService
  description: 'This service provides other top-level services with the current tag values of running jrobo instances'
#  termsOfService: http://prom-auto.ru/
  
  contact:
    email: d1-dev@mail.ru
    name: Denis Lobanov
  license:
    name: Apache 2.0
    url: http://www.apache.org/licenses/LICENSE-2.0.html
  version: 1.0.0
externalDocs:
  description: Find out more about JRobo
  url: http://prom-auto.ru/wiki/doku.php?id=doc:jroboplc:index
servers:
- url: http://localhost:8080/jrobo-data-ws

tags:
- name: tags
  description: 'A tag is a unit of data consisting of a name, value, data type, status, and update time'
- name: groups
  description: 'A group is a specific collection of tags'
- name: info
  description: 'Miscellaneous information'
- name: admin
  description: 'Admins stuff'

paths:
  /tags:
    get:
      tags:
      - tags
      summary: Finds tags by regex-pattern and return a sorted list of tags
      operationId: findAllTags
      parameters:
      - name: regex
        in: query
        description: Regex-pattern to search for tags
        required: false
        schema:
          type: string
          default: ".*"
        example: owen.*
      - name: limit
        in: query
        description: Page size
        required: false
        schema:
          type: integer
          default: 0
        example: 1000
      - name: next
        in: query
        description: The tag that starts the page, and if not found, starts with the nearest below
        required: false
        schema:
          type: string
          default: ""
        example: owen1.temperature
      responses:
        200:
          description: successful operation
          content:
            application/json:
              schema:
                title: TagsPageResponseDto
                description: A page of sorted list of tags (name and data)
                type: object
                properties:
                  next: 
                    description: Tag name to start the next page
                    type: string
                  tags:
                    $ref: '#/components/schemas/TagNameDataList'

                      
    put:                      
      tags:
      - tags
      summary: Write values for multiple tags
      operationId: writeTags
      requestBody:
        description: List of tags (name and value) to write
        required: true
        content:
          application/json: 
            schema:
              title: TagsWriteRequestDto
              type: object
              properties:
                tags:
                  $ref: '#/components/schemas/TagNameValueList'
      responses:
        202:
          description: Successful operation
          content:
            application/json: 
              schema:
                title: TagsWriteResponseDto
                type: object
                properties:
                  updated:
                    description: Number of tags written
                    type: integer

            
  /tags/{tagName}:
    get:
      tags:
      - tags
      summary: Finds one tag by its name
      operationId: findOneTag
      parameters:
      - name: tagName
        in: path
        description: Tag name to search 
        required: true
        schema:
          type: "string"
        example: owen.temperature
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/TagNameDataDto'
        404:
          description: Tag name not found
          content: {}


  /groups:
    post:                      
      tags:
      - groups
      summary: Create immutable group of tags by supplied list of tag names or using regex patterns. If the tags requested by the list do not exist, then they simply will not be in the group  
      operationId: createGroup
      requestBody:
        description: List of tag names and/or patterns
        required: true
        content:
          application/json: 
            schema:
              description: Criteria for selecting tags in a group 
              title: GroupCreateRequestDto
              type: object
              properties:
                tagNames:
                  $ref: '#/components/schemas/TagNameList'
      responses:
        201:
          description: Successful operation
          content:
            application/json: 
              schema:
                title: GroupCreateResponseDto
                description: List of tag names included in the group 
                type: object
                properties:
                  groupId:
                    description: Id of the created group
                    type: string


  /groups/{groupId}:
    get:
      tags:
      - groups
      summary: Finds one group
      operationId: findOneGroup
      parameters:
      - name: groupId
        in: path
        description: ID of the group
        required: true
        schema:
          type: string
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                title: GroupResponseDto
                description: List of tag names included in the group 
                type: object
                properties:
                  groupId:
                    description: Id of the created group
                    type: string
                  tagNames:
                    $ref: '#/components/schemas/TagNameList'
        404:
          description: Group not found
          content: {}
  
    delete:
      tags:
      - groups
      summary: Deletes a group
      operationId: deleteOneGroup
      parameters:
      - name: groupId
        in: path
        description: Id of the group
        required: true
        schema:
          type: string
      responses:
        202:
          description: Successful operation
          content: {}
        404:
          description: Group not found
          content: {}
  

  /groups/{groupId}/data:
    get:
      tags:
      - groups
      summary: 'Retrieve tag data (status, type, time, value of the group'
      operationId: retrieveGroupData
      parameters:
      - name: groupId
        in: path
        description: Id of the group
        required: true
        schema:
          type: string
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                title: GroupDataResponseDto
                description: List of tag values included in the group 
                type: object
                properties:
                  groupId:
                    description: ID of the created group
                    type: string
                  data:
                    $ref: '#/components/schemas/TagDataList'

        404:
          description: Group not found
          content: {}
            
  /info:
    get:
      tags:
      - info
      summary: Miscellaneous information
      operationId: getInfo
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                title: InfoResponseDto
                type: object
                properties:
                  connected:
                    description: Redis server connection status
                    type: boolean
                  server.time:
                    description: Redis server current time (UTS)
                    type: number
                  tags.stamp:
                    description: Time (UTS) when the tag table was last modified 
                    type: number
                  tags.size:
                    description: Tag table size
                    type: integer


  /admin/cache/{cacheName}:
    get:
      tags:
      - admin
      summary: Cache content
      operationId: getCache
      parameters:
      - name: cacheName
        in: path
        description: Cache name
        required: true
        schema:
          type: string
      responses:
        200:
          description: Successful operation
          content:
            application/json:
              schema:
                title: CacheResponseDto
                type: object
                properties:
                  cacheName:
                    type: string
                  content:
                    type: array
                    items:
                      type: string
        404:
          description: Unknown cache name
          content: {}

  /admin/cache/{cacheName}/stats:
    get:
      tags:
      - admin
      summary: Cache statistics
      operationId: getCacheStats
      parameters:
      - name: cacheName
        in: path
        description: Cache name
        required: true
        schema:
          type: string
      responses:
        200:
          $ref: '#/components/responses/CacheStatsResponse'
        404:
          description: Unknown cache name
          content: {}



components:
  responses:
    CacheStatsResponse:
      description: Successful operation
      content:
        application/json:
          schema:
            title: CacheStatsResponseDto
            type: object
            properties:
              cacheName:
                type: string
              stats:
                $ref: '#/components/schemas/CacheStatsDto'
                


  schemas:
    TagName:
      type: string

    TagValue:
      type: string

    TagData:
      description: 'Tag data: good#1  type#1  time#11 space#1 value#?'
      type: string


    TagNameDataDto:
      type: object
      properties:
        name:
          $ref: '#/components/schemas/TagName'
        data:
          $ref: '#/components/schemas/TagData'


    TagNameValueDto:
      type: object
      properties:
        name:
          $ref: '#/components/schemas/TagName'
        value:
          $ref: '#/components/schemas/TagValue'
          
          

    TagNameList:
      type: array
      items:
        $ref: '#/components/schemas/TagName'

    TagDataList:
      type: array
      items:
        $ref: '#/components/schemas/TagData'

    TagNameDataList:
      type: array
      items:
        $ref: '#/components/schemas/TagNameDataDto'

    TagNameValueList:
      type: array
      items:
        $ref: '#/components/schemas/TagNameValueDto'

      
    CacheStatsDto:
      type: object
      properties:
        averageLoadPenalty:
          description: 'Returns the average time spent loading new values'
          type: number
        evictionCount:
          description: 'Returns the number of times an entry has been evicted'
          type: number
        hitCount:
          description: 'Returns the number of times Cache lookup methods have returned a cached value'
          type: number
        hitRate:
          description: 'Returns the ratio of cache requests which were hits'
          type: number
        loadCount:
          description: 'Returns the total number of times that Cache lookup methods attempted to load new values'
          type: number
        loadExceptionCount:
          description: 'Returns the number of times Cache lookup methods threw an exception while loading a new value.'
          type: number
        loadExceptionRate:
          description: 'Returns the ratio of cache loading attempts which threw exceptions.'
          type: number
        loadSuccessCount:
          description: 'Returns the number of times Cache lookup methods have successfully loaded a new value'
          type: number
        missCount:
          description: 'Returns the number of times Cache lookup methods have returned an uncached (newly loaded) value, or null'
          type: number
        missRate:
          description: 'Returns the ratio of cache requests which were misses'
          type: number
        requestCount:
          description: 'Returns the number of times Cache lookup methods have returned either a cached or uncached value'
          type: number
        totalLoadTime:
          description: 'Returns the total number of nanoseconds the cache has spent loading new values'
          type: number


