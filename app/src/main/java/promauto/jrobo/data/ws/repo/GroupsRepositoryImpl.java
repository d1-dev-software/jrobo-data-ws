package promauto.jrobo.data.ws.repo;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheStats;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import promauto.jrobo.data.ws.model.Group;
import promauto.jrobo.data.ws.properties.AppProperties;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Slf4j
@Repository
public class GroupsRepositoryImpl implements GroupsRepository {

    final Cache<String, Group> cacheGroups;
    final Cache<Integer, String> cacheGroupIds;

    @Autowired
    public GroupsRepositoryImpl(AppProperties properties) {
        var cacheGroupsBuilder = CacheBuilder.newBuilder()
//                .removalListener(ntf -> System.out.println("===removed from groups " + ntf.getCause() + ", " + ntf.getKey() + " = " + ntf.getValue()))
//                .maximumSize(3)
                .maximumSize(properties.cacheMaximumSize)
                .expireAfterAccess(properties.cacheExpireSeconds, TimeUnit.SECONDS);

        if( properties.cacheRecordStats )
            cacheGroupsBuilder.recordStats();

        cacheGroups = cacheGroupsBuilder.build();

        cacheGroupIds = CacheBuilder.newBuilder()
//                .removalListener(ntf -> System.out.println("---removed from cache  " + ntf.getCause() + ", " + ntf.getKey() + " = " + ntf.getValue()))
                .weakValues()
                .build();
    }


    @Override
    public Group create(List<String> tagNames) {
        Group group = null;

        String groupId = cacheGroupIds.getIfPresent(tagNames.hashCode());
        if( groupId != null ) {
            group = cacheGroups.getIfPresent(groupId);
        }

        if( group == null  ||  !group.tagnames.equals(tagNames) ) {
            group = new Group(UUID.randomUUID().toString(), Collections.unmodifiableList(tagNames));
            cacheGroups.put(group.id, group);
            cacheGroupIds.put(group.tagnames.hashCode(), group.id);
        }
        return group;
    }


    @Override
    public Optional<Group> get(String id) {
        Group group = cacheGroups.getIfPresent(id);
        return group == null? Optional.empty(): Optional.of(group);
    }

    @Override
    public boolean remove(String groupId) {
        boolean found = cacheGroups.getIfPresent(groupId) != null;
        cacheGroups.invalidate(groupId);
        return found;
    }

    @Override
    public List<String> getCacheContent() {
        return new ArrayList<>(cacheGroups.asMap().keySet());
    }

    @Override
    public CacheStats getCacheStats() {
        return cacheGroups.stats();
    }


}
