package promauto.jrobo.data.ws.repo;

import com.google.common.cache.CacheStats;
import promauto.jrobo.data.ws.model.Group;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface GroupsRepository {

    Group create(List<String> tagNames);

    Optional<Group> get(String id);

    boolean remove(String groupId);

    List<String> getCacheContent();

    CacheStats getCacheStats();
}
