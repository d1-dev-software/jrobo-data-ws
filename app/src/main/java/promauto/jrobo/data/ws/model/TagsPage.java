package promauto.jrobo.data.ws.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter @Setter
@AllArgsConstructor
public class TagsPage {
    private List<TagNameData> tags = null;
    private String next;

}
