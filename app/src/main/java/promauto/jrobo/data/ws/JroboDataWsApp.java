package promauto.jrobo.data.ws;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import promauto.jrobo.data.ws.properties.AppProperties;
import promauto.jrobo.data.ws.service.KeeperService;

@EnableAsync
@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class JroboDataWsApp {

	public static void main(String[] args) {
		SpringApplication.run(JroboDataWsApp.class, args);
	}

	@Bean
	public CommandLineRunner startDataClient(KeeperService keeper) {
		return args -> keeper.start();
	}

}

