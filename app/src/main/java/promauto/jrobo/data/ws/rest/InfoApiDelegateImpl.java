package promauto.jrobo.data.ws.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import promauto.jrobo.data.ws.api.InfoApiDelegate;
import promauto.jrobo.data.ws.model.InfoResponseDto;
import promauto.jrobo.data.ws.repo.TagsRepository;
import promauto.jrobo.data.ws.service.KeeperService;

import java.math.BigDecimal;

@Service
public class InfoApiDelegateImpl implements InfoApiDelegate {
    private final TagsRepository repo;
    private final KeeperService keeper;

    @Autowired
    public InfoApiDelegateImpl(TagsRepository repo, KeeperService keeper) {
        this.repo = repo;
        this.keeper = keeper;
    }

    @Override
    public ResponseEntity<InfoResponseDto> getInfo() {
        return new ResponseEntity<>(
                new InfoResponseDto()
                        .connected(keeper.isConnected())
                        .serverTime(new BigDecimal(keeper.getServerTime()))
                        .tagsSize(repo.size())
                        .tagsStamp(new BigDecimal(repo.getStorageModifiedStamp())),
                HttpStatus.OK);
    }
}
