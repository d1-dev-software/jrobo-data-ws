package promauto.jrobo.data.ws.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import promauto.jrobo.data.ws.api.TagsApiDelegate;
import promauto.jrobo.data.ws.model.*;
import promauto.jrobo.data.ws.service.TagsService;

import java.util.stream.Collectors;

@Service
public class TagsApiDelegateImpl implements TagsApiDelegate {
    TagsService tagsService;

    @Autowired
    public TagsApiDelegateImpl(TagsService tagsService) {
        this.tagsService = tagsService;
    }

    @Override
    public ResponseEntity<TagsPageResponseDto> findAllTags(String regex, Integer limit, String next) {
        TagsPage page = tagsService.getTagsPage(regex, limit, next);
        return new ResponseEntity<>(
                new TagsPageResponseDto()
                        .next(page.getNext())
                        .tags(page.getTags().stream()
                                .map(tag -> new TagNameDataDto()
                                        .name(tag.getName())
                                        .data(tag.getData()))
                                .collect(Collectors.toList())),
                HttpStatus.OK);
    }


    @Override
    public ResponseEntity<TagNameDataDto> findOneTag(String tagName) {
        return tagsService.getTagNameData(tagName)
                .map(tag -> new ResponseEntity<>(
                        new TagNameDataDto()
                                .name(tag.getName())
                                .data(tag.getData()),
                        HttpStatus.OK
                )).orElse(new ResponseEntity<>(
                        HttpStatus.NOT_FOUND
                ));
    }


    @Override
    public ResponseEntity<TagsWriteResponseDto> writeTags(TagsWriteRequestDto tagsWriteRequestDto) {
        int cnt = (int)tagsWriteRequestDto.getTags().stream()
                .map(tag -> tagsService.writeTagValue(tag.getName(), tag.getValue())? 1: 0)
                .count();

        return new ResponseEntity<>(new TagsWriteResponseDto().updated(cnt), HttpStatus.ACCEPTED);
    }
}
