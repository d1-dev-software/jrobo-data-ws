package promauto.jrobo.data.ws.service;

import com.google.common.cache.CacheStats;
import promauto.jrobo.data.ws.model.TagNameData;
import promauto.jrobo.data.ws.model.TagsPage;

import java.util.List;
import java.util.Optional;

public interface TagsService {
    TagsPage getTagsPage(String pattern, Integer limit, String next);

    Optional<TagNameData> getTagNameData(String tagName);

    boolean writeTagValue(String tagname, String value);

    List<String> getCacheContent();

    CacheStats getCacheStats();
}
