package promauto.jrobo.data.ws.probe;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class ExpensiveService {

    @Cacheable("expensive")
    public String act(int i) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "i=" + i + ", time=" + System.currentTimeMillis();
    }

    @Async
    public void count(String msg) {
        System.out.println("---");
        for(int i=0; i<5; ++i) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(msg + " - " + i);
        }
    }

}
