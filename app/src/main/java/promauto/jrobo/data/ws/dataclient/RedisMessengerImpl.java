package promauto.jrobo.data.ws.dataclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import promauto.jrobo.data.ws.protocol.Protocol;
import promauto.jrobo.data.ws.service.KeeperService;
import promauto.jrobo.data.ws.shared.TagInfo;

@Component
public class RedisMessengerImpl implements RedisMessenger {

    RedisClient redisClient;
    KeeperService keeper;
    Protocol protocol;

    @Autowired
    public RedisMessengerImpl(RedisClient redisClient, KeeperService keeper, Protocol protocol) {
        this.redisClient = redisClient;
        this.keeper = keeper;
        this.protocol = protocol;

        redisClient.addListenerOnConnected(() -> {
            redisClient.addPubSubListener(this);
            redisClient.subscribePubSub(protocol.getUpdateChannelName(), protocol.getDeleteChannelName());
        });

    }


    @Override
    public void message(String channel, String message) {
        if( channel.equals( protocol.getUpdateChannelName() )) {
            TagInfo ti = protocol.decodeMessageUpdate(message);
            if( ti != null ) {
                keeper.updateTag(ti.name, ti.time, ti.data);
            }
        } else if( channel.equals( protocol.getDeleteChannelName() )) {
            TagInfo ti = protocol.decodeMessageDelete(message);
            if( ti != null ) {
                keeper.deleteTag(ti.name, ti.time);
            }
        }
    }


    @Override
    public void sendMessageWrite(String tagname, String value) {
        String message = protocol.encodeMessageWrite(tagname, value);
        redisClient.asyncPublish(protocol.getWriteChannelName(), message);
    }

}
