package promauto.jrobo.data.ws.protocol;

import promauto.jrobo.data.ws.shared.TagInfo;

public interface Protocol {

    String getUpdateChannelName();

    String getDeleteChannelName();

    String getWriteChannelName();

    String getTagsKey();

    String getServerTimeKey();

    long decodeTimeFromData(String data);

    String encodeMessageWrite(String name, String value);

    TagInfo decodeMessageUpdate(String message);

    TagInfo decodeMessageDelete(String message);

}
