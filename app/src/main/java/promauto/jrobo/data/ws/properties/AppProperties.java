package promauto.jrobo.data.ws.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

@Getter @Setter
@ConfigurationProperties(prefix = "jrobo")
public class AppProperties {

    @NestedConfigurationProperty
    public RedisProperties redis = new RedisProperties();

    public String domain = "";
    public String tagsFilter = "*";
    public boolean readonly = false;

    public long cacheMaximumSize = 10000;
    public long cacheExpireSeconds = 600;
    public boolean cacheRecordStats = true;

    public int keeperCyclePeriodMs = 1000;
    public int keeperChunkUpdatePeriodS = 600;


    @Getter @Setter
    public static class RedisProperties {

        public String clientName = "jrobo-data-ws";
        public String host = "localhost";
        public String username = "";
        public String password = "";
        public int port = 6379;
        public int dbnum = 0;
        public boolean ssl = false;
        public int connectionTimeoutS = 10;
        public int commandTimeoutS = 60;

    }

}
