package promauto.jrobo.data.ws.shared;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheStats;
import lombok.AllArgsConstructor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PagedStorage<V> {

    static final int PAGE_LIMIT_MAX = 10000;
//    static final long CACHE_MAXIMUM_SIZE = 10000;
//    static final long CACHE_EXPIRE_SECONDS = 60;

    @AllArgsConstructor
    public static class Page {
        public final List<String> keys;
        public final String nextPageStartKey;

        public static Page emptyPage() {
            return new Page(Collections.emptyList(), "");
        }
    }

    final ConcurrentMap<String,V> items = new ConcurrentHashMap<>();
    final Cache<String, Page> cache;

    private volatile List<String> keys = null;
    private volatile boolean needUpdateKeys = true;
    private volatile long storageModifiedStamp = 0;


//    public PagedStorage() {
//        this(CACHE_MAXIMUM_SIZE, CACHE_EXPIRE_SECONDS, true);
//    }

    public PagedStorage(long cacheMaximumSize, long cacheExpire, boolean recordStats) {
        var cacheBuilder = CacheBuilder.newBuilder()
//                .maximumSize(3)
//                .expireAfterAccess(30, TimeUnit.SECONDS)
//                .concurrencyLevel()
//                .initialCapacity()
                .maximumSize(cacheMaximumSize)
                .expireAfterAccess(cacheExpire, TimeUnit.SECONDS);
//                .removalListener(ntf -> System.out.println("removed " + ntf.getCause() + ", " + ntf.getKey() + " = " + ntf.getValue()))

        if( recordStats )
            cacheBuilder.recordStats();

        cache = cacheBuilder.build();
    }

    public V getValue(String key) {
        return items.get(key);
    }


//    public void setValue(String key, V value) {
//        items.replace(key, value);
//    }


    public void put(String key, V value) {
        if( items.put(key, value) == null )
            setNeedUpdateKeys();
    }


    public void remove(String key) {
        if( items.remove(key) != null )
            setNeedUpdateKeys();
    }


    public void retainAll(Set<String> keysToRetain) {
        items.keySet().retainAll(keysToRetain);
        setNeedUpdateKeys();
    }


    public Page getPage(String regex, int limit, String next) {
        updateKeys();

        if( regex==null  ||  regex.isEmpty() )
            regex = ".*";

        if( next==null )
            next = "";

        String pageId = regex;
        if( limit > 0)
            pageId += " #" + limit;

        if( !next.isEmpty() )
            pageId += " ^" + next;

        try {
            String localRegex = regex;
            String localNext = next;
            return cache.get(pageId, () -> createPage(localRegex, limit, localNext));
        } catch (Exception e) {
            return Page.emptyPage();
        }
    }


    private Page createPage(String regex, int limit, String next) {
        Matcher matcher = null;
        if( !regex.equals(".*") ) {
            matcher = Pattern.compile(regex).matcher("");
        }

        // The list of keys is immutable, but its reference is not,
        // so we must use a local copy of the keys reference
        var localKeys = this.keys;
        var pageKeys = new ArrayList<String>();

        limit = Math.min(
                limit == 0?
                        localKeys.size():
                        limit,
                PAGE_LIMIT_MAX);

        int i = getNearestIndex(localKeys, next);
        int n = 0;
        next = "";
        for(; i<localKeys.size(); ++i) {
            String key = localKeys.get(i);
            if( matcher != null  &&  !matcher.reset(key).matches() )
                continue;
            if( n++ < limit ) {
                pageKeys.add(key);
            } else {
                next = key;
                break;
            }
        }
        return new Page(Collections.unmodifiableList(pageKeys), next);
    }


    public int size() {
        updateKeys();
        return keys.size();
    }


    public long getStorageModifiedStamp() {
        updateKeys();
        return storageModifiedStamp;
    }


    public List<String> getKeySublist(int from, int size) {
        updateKeys();
        var localKeys = keys;
        return localKeys.isEmpty()?
                new ArrayList<>():
                localKeys.subList(from, Math.min(localKeys.size(), from + size));
    }


    private synchronized void setNeedUpdateKeys() {
        needUpdateKeys = true;
    }


    private void updateKeys() {
        if (needUpdateKeys) {
            synchronized (this) {
                if (needUpdateKeys) {
                    // build new keys
                    var localKeys = new ArrayList<>(items.keySet());
                    Collections.sort(localKeys);
                    this.keys = Collections.unmodifiableList(localKeys);

                    // clear cache
                    cache.invalidateAll();

                    storageModifiedStamp = System.currentTimeMillis();
                    needUpdateKeys = false;
                }
            }
        }
    }


    private int getNearestIndex(List<String> list, String key) {
        if( key == null  ||  key.isEmpty() )
            return 0;

        int i = Collections.binarySearch(list, key);
        return i < 0? -(i + 1): i;
    }


    public List<String> getCacheContent() {
        return new ArrayList<>(cache.asMap().keySet());
    }

    public CacheStats getCacheStats() {
        return cache.stats();
    }


}
