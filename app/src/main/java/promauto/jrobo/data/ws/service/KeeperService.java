package promauto.jrobo.data.ws.service;

import org.springframework.scheduling.annotation.Async;

public interface KeeperService {
    @Async
    void start();

    void updateTag(String tagname, long time, String data);

    void deleteTag(String tagname, long time);

    Boolean isConnected();

    long getServerTime();
}
