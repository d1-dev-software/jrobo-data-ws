package promauto.jrobo.data.ws.service;

import com.google.common.cache.CacheStats;
import org.springframework.stereotype.Service;
import promauto.jrobo.data.ws.dataclient.RedisMessenger;
import promauto.jrobo.data.ws.model.TagNameData;
import promauto.jrobo.data.ws.model.TagTimeData;
import promauto.jrobo.data.ws.model.TagsPage;
import promauto.jrobo.data.ws.repo.TagsRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TagsServiceImpl implements TagsService {

    private final RedisMessenger messenger;
    private final TagsRepository repo;

    public TagsServiceImpl(RedisMessenger messenger, TagsRepository repo) {
        this.messenger = messenger;
        this.repo = repo;
    }

    @Override
    public TagsPage getTagsPage(String pattern, Integer limit, String next) {
        var page = repo.getPage(pattern, limit, next);

        var tags = page.keys.stream()
                .map(tagname -> new TagNameData(tagname, repo.getValue(tagname).data))
                .filter(tagNameData -> tagNameData.getData() != null)
                .collect(Collectors.toList());

        return new TagsPage(tags, page.nextPageStartKey);
    }


    @Override
    public Optional<TagNameData> getTagNameData(String tagName) {
        TagTimeData td = repo.getValue(tagName);
        return td == null?
                Optional.empty():
                Optional.of(new TagNameData(tagName, td.data));
    }


    @Override
    public boolean writeTagValue(String tagname, String value) {
        if( repo.getValue(tagname) == null )
            return false;
        messenger.sendMessageWrite(tagname, value);
        return true;
    }


    @Override
    public List<String> getCacheContent() {
        return repo.getCacheContent();
    }


    @Override
    public CacheStats getCacheStats() {
        return repo.getCacheStats();
    }

}
