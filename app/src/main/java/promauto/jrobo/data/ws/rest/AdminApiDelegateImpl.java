package promauto.jrobo.data.ws.rest;

import com.google.common.cache.CacheStats;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import promauto.jrobo.data.ws.api.AdminApiDelegate;
import promauto.jrobo.data.ws.model.CacheResponseDto;
import promauto.jrobo.data.ws.model.CacheStatsDto;
import promauto.jrobo.data.ws.model.CacheStatsResponseDto;
import promauto.jrobo.data.ws.service.GroupsService;
import promauto.jrobo.data.ws.service.TagsService;

import java.math.BigDecimal;

@Service
public class AdminApiDelegateImpl implements AdminApiDelegate {
    private final TagsService tagsService;
    private final GroupsService groupsService;

    @Autowired
    public AdminApiDelegateImpl(TagsService tagsService, GroupsService groupsService) {
        this.tagsService = tagsService;
        this.groupsService = groupsService;
    }


    @Override
    public ResponseEntity<CacheResponseDto> getCache(String cacheName) {
        var dto = new CacheResponseDto()
                .cacheName(cacheName)
                .content(
                        switch (cacheName.toLowerCase()) {
                            case "tags" -> tagsService.getCacheContent();
                            case "groups" -> groupsService.getCacheContent();
                            default -> null;
                        }
                );

        return dto.getContent() == null?
                new ResponseEntity<>(HttpStatus.BAD_REQUEST):
                new ResponseEntity<>(dto, HttpStatus.OK);
    }


    @Override
    public ResponseEntity<CacheStatsResponseDto> getCacheStats(String cacheName) {
        var dto = new CacheStatsResponseDto()
                .cacheName(cacheName)
                .stats(
                        switch (cacheName.toLowerCase()) {
                            case "tags" -> toCacheStatsDto( tagsService.getCacheStats() );
                            case "groups" -> toCacheStatsDto( groupsService.getCacheStats() );
                            default -> null;
                        }
                );

        return dto.getStats() == null?
            new ResponseEntity<>(HttpStatus.BAD_REQUEST):
            new ResponseEntity<>(dto, HttpStatus.OK);
    }


    private CacheStatsDto toCacheStatsDto(CacheStats stats) {
        return new CacheStatsDto()
                .averageLoadPenalty(BigDecimal.valueOf( stats.averageLoadPenalty() ))
                .evictionCount(     BigDecimal.valueOf( stats.evictionCount() ))
                .hitCount(          BigDecimal.valueOf( stats.hitCount() ))
                .hitRate(           BigDecimal.valueOf( stats.hitRate() ))
                .loadCount(         BigDecimal.valueOf( stats.loadCount() ))
                .loadExceptionCount(BigDecimal.valueOf( stats.loadExceptionCount() ))
                .loadExceptionRate( BigDecimal.valueOf( stats.loadExceptionRate() ))
                .loadSuccessCount(  BigDecimal.valueOf( stats.loadSuccessCount() ))
                .missCount(         BigDecimal.valueOf( stats.missCount() ))
                .missRate(          BigDecimal.valueOf( stats.missRate() ))
                .requestCount(      BigDecimal.valueOf( stats.requestCount() ))
                .totalLoadTime(     BigDecimal.valueOf( stats.totalLoadTime() ));
    }
}
