package promauto.jrobo.data.ws.dataclient;

import io.lettuce.core.*;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.pubsub.StatefulRedisPubSubConnection;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import promauto.jrobo.data.ws.properties.AppProperties;

import javax.annotation.PreDestroy;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

@Slf4j
@Service
public class RedisClientImpl implements RedisClient {

    public static final int RECONNECT_DELAY_MS = 3000;

    AppProperties properties;

    @Autowired
    public RedisClientImpl(AppProperties properties) {
        this.properties = properties;
    }


    private volatile StatefulRedisConnection<String,String> connectionCmd;
    private volatile StatefulRedisPubSubConnection<String,String> connectionPubSub;
    private final List<Runnable> onConnectedListeners = new ArrayList<>();


    @Async
    @Override
    public void start() {
        io.lettuce.core.RedisClient redisClient = getRedisClient();
        RedisURI uri = buildRedisURI();
        log.info("Redis client connecting: {}", uri.toString());

        for(;;) {
            try {
                if( connectionCmd == null ) {
                    try {
                        connectionCmd = redisClient.connect(uri);
                        connectionPubSub = redisClient.connectPubSub(uri);
                        runListenersOnConnected();

                        log.info("Redis client connected successfully");
                        break;
                    } catch (Exception e) {
                        disconnect();
                        log.warn("Redis client failed to connect: {}", e.getLocalizedMessage());
                    }
                }
                Thread.sleep(RECONNECT_DELAY_MS);
            } catch (InterruptedException e) {
                break;
            }
        }
    }


    private io.lettuce.core.RedisClient getRedisClient() {
        //  todo: use native epoll
//  https://github.com/lettuce-io/lettuce-core/issues/1428
//        ClientResources clientResources = ClientResources.builder()
//                .nettyCustomizer(new NettyCustomizer() {
//                    @Override
//                    public void afterBootstrapInitialized(Bootstrap bootstrap) {
//                        bootstrap.option(EpollChannelOption.TCP_KEEPIDLE, 15);
//                        bootstrap.option(EpollChannelOption.TCP_KEEPINTVL, 5);
//                        bootstrap.option(EpollChannelOption.TCP_KEEPCNT, 3);
//                        // Socket Timeout (milliseconds)
//                        bootstrap.option(EpollChannelOption.TCP_USER_TIMEOUT, 60000);
//                    }
//                })
//                .build();
//        redisClient = RedisClient.create(clientResources);

        SocketOptions socketOptions = SocketOptions.builder()
                .connectTimeout(Duration.ofSeconds(2))
                .keepAlive(true)
                .build();
        ClientOptions clientOptions = ClientOptions.builder()
                .socketOptions(socketOptions)
                .build();

        io.lettuce.core.RedisClient redisClient = io.lettuce.core.RedisClient.create();
        redisClient.setOptions(clientOptions);
        return redisClient;
    }


    private RedisURI buildRedisURI() {
        RedisURI.Builder uriBuilder = RedisURI.builder()
                .withHost(properties.redis.host)
                .withPort(properties.redis.port)
                .withDatabase(properties.redis.dbnum)
                .withSsl(properties.redis.ssl);

        if( !properties.redis.clientName.isEmpty() )
            uriBuilder = uriBuilder.withClientName(properties.redis.clientName);

        if( !properties.redis.username.isEmpty() )
            uriBuilder = uriBuilder.withAuthentication(properties.redis.username, properties.redis.password);

        else if( properties.redis.password.length() > 0 ) {
            CharSequence psw = properties.redis.password;
            uriBuilder = uriBuilder.withPassword(psw);
        }

        return uriBuilder.build();
    }


    @PreDestroy
    void disconnect() {
        if(connectionCmd != null)
            connectionCmd.closeAsync();

        if(connectionPubSub != null)
            connectionPubSub.closeAsync();
    }


    private synchronized void runListenersOnConnected() {
        onConnectedListeners.forEach(Runnable::run);
    }

    @Override
    public synchronized void addListenerOnConnected(Runnable listener) {
        onConnectedListeners.add(listener);
    }


    @Override
    public void addPubSubListener(RedisMessenger listener) {
        connectionPubSub.addListener(listener);
    }


    @Override
    public void subscribePubSub(String... channels) {
        connectionPubSub.async().subscribe(channels);
    }

    @Override
    public boolean isConnected() {
        return connectionCmd != null  &&  connectionCmd.isOpen();
    }

    @Override
    public CompletableFuture<String> asyncGetStr(String key) {
        return connectionCmd.async().get(key)
                .toCompletableFuture();
    }

    @Override
    public CompletableFuture<Long> asyncHScanStr(String key, String filter, KeyValueScanHandler onKeyValueScan) {
        ScanArgs args = ScanArgs.Builder
                .matches(filter)
                .limit(Long.MAX_VALUE);

        return connectionCmd.async().hscan(onKeyValueScan::accept, key, args)
                .thenApply(StreamScanCursor::getCount)
                .toCompletableFuture();
    }

    @Override
    public CompletableFuture<String> asyncHGetStr(String key, String field) {
        return connectionCmd.async().hget(key, field)
                .toCompletableFuture();
    }

    @Override
    public CompletableFuture<Long> asyncPublish(String channel, String message) {
        return connectionCmd.async().publish(channel, message).toCompletableFuture();
    }

}
