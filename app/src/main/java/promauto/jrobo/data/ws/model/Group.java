package promauto.jrobo.data.ws.model;

import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class Group {
    public final String id;
    public final List<String> tagnames;
}
