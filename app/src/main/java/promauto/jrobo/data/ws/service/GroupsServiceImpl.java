package promauto.jrobo.data.ws.service;

import com.google.common.cache.CacheStats;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import promauto.jrobo.data.ws.model.Group;
import promauto.jrobo.data.ws.repo.GroupsRepository;
import promauto.jrobo.data.ws.repo.TagsRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class GroupsServiceImpl implements GroupsService {

    private final TagsRepository tagsRepo;
    private final GroupsRepository groupsRepo;


    @Autowired
    public GroupsServiceImpl(TagsRepository tagsRepo, GroupsRepository groupsRepo) {
        this.tagsRepo = tagsRepo;
        this.groupsRepo = groupsRepo;
    }


    @Override
    public Group createGroup(List<String> tagNames) {
        return groupsRepo.create( tagNames );
    }

    @Override
    public Optional<Group> getGroup(String groupId) {
        return groupsRepo.get(groupId);
    }

    @Override
    public boolean removeGroup(String groupId) {
        return groupsRepo.remove(groupId);
    }

    @Override
    public Optional<List<String>> getGroupData(String groupId) {
        var group = groupsRepo.get(groupId);
        if( group.isEmpty() )
            return Optional.empty();

        var tagnames = group.get().tagnames;
        List<String> data = new ArrayList<>(tagnames.size());
        tagnames.forEach(tagname -> {
            var td = tagsRepo.getValue(tagname);
            data.add( td==null? null: td.data);
        });

        return Optional.of(data);
    }

    @Override
    public List<String> getCacheContent() {
        return groupsRepo.getCacheContent();
    }

    @Override
    public CacheStats getCacheStats() {
        return groupsRepo.getCacheStats();
    }
}
