package promauto.jrobo.data.ws.dataclient;

import io.lettuce.core.pubsub.RedisPubSubListener;

public interface RedisMessenger extends RedisPubSubListener<String,String> {

    void sendMessageWrite(String tagname, String data);

    void message(String channel, String message);


    default void message(String pattern, String channel, String message) {
        // do nothing
    }

    default void subscribed(String channel, long count) {
        // do nothing
    }

    default void psubscribed(String pattern, long count) {
        // do nothing
    }

    default void unsubscribed(String channel, long count) {
        // do nothing
    }

    default void punsubscribed(String pattern, long count) {
        // do nothing
    }

}
