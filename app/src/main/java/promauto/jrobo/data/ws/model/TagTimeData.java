package promauto.jrobo.data.ws.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class TagTimeData {
    public final long time;
    public final String data;
}
