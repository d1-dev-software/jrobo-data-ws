package promauto.jrobo.data.ws.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import promauto.jrobo.data.ws.model.TagTimeData;
import promauto.jrobo.data.ws.properties.AppProperties;
import promauto.jrobo.data.ws.shared.PagedStorage;

@Repository
public class TagsRepository extends PagedStorage<TagTimeData> {


    @Autowired
    public TagsRepository(AppProperties properties) {
        super(
                properties.cacheMaximumSize,
                properties.cacheExpireSeconds,
                properties.cacheRecordStats
        );
    }
}
