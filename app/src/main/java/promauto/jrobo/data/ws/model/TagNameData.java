package promauto.jrobo.data.ws.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor
public class TagNameData {
    private String name;
    private String data;
}
