package promauto.jrobo.data.ws.protocol;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import promauto.jrobo.data.ws.properties.AppProperties;
import promauto.jrobo.data.ws.shared.TagInfo;
import promauto.jrobo.data.ws.shared.TagInfoCodec;

@Component
public class ProtocolImpl implements Protocol{

    private final String channelUpdate;
    private final String channelDelete;
    private final String channelWrite;
    private final String tagsKey;
    private final String serverTimeKey;

    @Autowired
    public ProtocolImpl(AppProperties properties) {
        String domain = properties.domain.trim();
        if( !domain.isEmpty() )
            domain += ".";

        channelUpdate = domain + TagInfoCodec.CHANNEL_UPDATE;
        channelDelete = domain + TagInfoCodec.CHANNEL_DELETE;
        channelWrite = domain + TagInfoCodec.CHANNEL_WRITE;
        tagsKey = domain + TagInfoCodec.TAGS_KEY;
        serverTimeKey = domain + TagInfoCodec.SERVER_TIME_KEY;
    }


    @Override
    public String getUpdateChannelName() {
        return channelUpdate;
    }

    @Override
    public String getDeleteChannelName() {
        return channelDelete;
    }

    @Override
    public String getWriteChannelName() {
        return channelWrite;
    }

    @Override
    public String getTagsKey() {
        return tagsKey;
    }

    @Override
    public String getServerTimeKey() {
        return serverTimeKey;
    }

    @Override
    public String encodeMessageWrite(String name, String value) {
        return TagInfoCodec.encodeMessageWrite(name, value);
    }

    @Override
    public long decodeTimeFromData(String data) {
        return TagInfoCodec.decodeTimeFromData(data);
    }

    @Override
    public TagInfo decodeMessageUpdate(String message) {
        return TagInfoCodec.decodeMessageUpdate(message);
    }

    @Override
    public TagInfo decodeMessageDelete(String message) {
        return TagInfoCodec.decodeMessageDelete(message);
    }
}
