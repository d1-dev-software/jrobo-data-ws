package promauto.jrobo.data.ws.probe;


import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Probe {
    private static long t1, t2, t3, t4;

    public static void main(String[] args)  {
        List<String> a = new ArrayList<>();
        a.add("aaa");
        a.add("bbb");
        a.set(0, a.get(0)+"a");
        a.set(1, a.get(1)+"b");
        System.out.println(a.hashCode());

        List<String> b = new LinkedList<>();
        b.add("aaaa");
        b.add("bbbb");
        System.out.println(b.hashCode());
        b = Collections.unmodifiableList(b);
        System.out.println(b.hashCode());

        System.out.println( a.equals(b));
    }

    public static void main2(String[] args)  {
        String[] src1 = new String[]{};
        String[] src2 = new String[]{"test1"};
        String[] src3 = new String[]{"", "test1", "te", "test", "test22", "test333", "tes-1"};
        String[] src4 = new String[]{"tt", "test1", "", "test", "test22", "test333", "tes-1"};
        String[] src5 = new String[]{"tt", "test1", "te", "test", "test22", "test333", "tes-1"};
        String[] src6 = new String[]{"est1", "test1", "test", "test22", "test333", "tes-1"};
        String[] src7 = new String[]{"test1", "test1", "test1", "test1", "test1"};

//        System.out.println( extractPrefix(src1) );
//        System.out.println( extractPrefix(src2) );
        System.out.println( extractPrefix(src3) );
        System.out.println( extractPrefix(src4) );
        System.out.println( extractPrefix(src5) );
        System.out.println( extractPrefix(src6) );
        System.out.println( extractPrefix(src7) );

    }

    private static String extractPrefix5(String[] strs) {
        int index = 0;

        int n = 0;
        chars:
        for (; ; index++) {
            for (int i = 0; i < strs.length - 1; i++) {
                n++;
                if ((strs[i].length() == index || strs[i + 1].length() == index)
                        || strs[i].charAt(index) != strs[i + 1].charAt(index)) {
                    break chars;
                }
            }
        }

        System.out.print("n = " + n + "   ");
        return strs[0].substring(0, index);
    }

    private static String extractPrefix(String[] src) {
        if( src == null || src.length == 0 )
            return "";
        if( src.length == 1 )
            return src[0];

        int n = 0;
        int i = 0;
        loop:
        for(; i < src[0].length(); ++i) {
            for(int row=1; row<src.length; ++row) {
                n++;
                if(i >= src[row].length()  ||  src[0].charAt(i) != src[row].charAt(i)) {
                    break loop;
                }
            }
        }
        System.out.print("n = " + n + "   ");
        return src[0].substring(0, i);
    }


    private static String extractPrefix1(String[] src) {
        if( src == null || src.length == 0 )
            return "";
        if( src.length == 1 )
            return src[0];

        String s = src[0];
        boolean hasMore = true;
        int i = 0;
        for(; i < s.length() && hasMore; ++i) {
            boolean hasDiff = false;
            for(int j=1; j<src.length; ++j) {
                hasDiff = i < src[j].length()  &&  s.charAt(i) != src[j].charAt(i);
                hasMore &= i + 1 < src[j].length();
            }
            if( hasDiff )
                break;
        }
        return s.substring(0, i);
    }


    public static void main1(String[] args) throws InterruptedException, IOException, ExecutionException {
        LoadingCache<String, String> cache = CacheBuilder.newBuilder()
                .maximumSize(3)
                .expireAfterAccess(5, TimeUnit.SECONDS)
                .removalListener(notification -> System.out.println("removed: " + notification.getCause()))
                .build(
                        new CacheLoader<String, String>() {
                            @Override
                            public String load(String key) throws Exception {
                                return "=== " + key + " === " + (System.currentTimeMillis() % 1000);
                            }
                        });

        Cache<String, String> cache1 = CacheBuilder.newBuilder()
                .maximumSize(3)
                .expireAfterAccess(5, TimeUnit.SECONDS)
                .removalListener(ntf -> System.out.println("removed " + ntf.getCause() + ", " + ntf.getKey() + " = " + ntf.getValue()))
                .build();

        System.out.println(">");
        Scanner sc = new Scanner(System.in);
        for (; ; ) {
            String str = sc.nextLine();
            if (str.equals("`"))
                break;


//            String res = cache.get(str);

            String value = "" + (System.currentTimeMillis()%10000);
            String res = cache1.get(str, () -> {
                return value;
            });

            System.out.println(str + " = " + res);
        }
    }
}
