package promauto.jrobo.data.ws.dataclient;

import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;

public interface RedisClient {

    interface KeyValueScanHandler extends BiConsumer<String,String> {
        void accept(String key, String value);
    }

    void start();

    void addListenerOnConnected(Runnable listener);

    void addPubSubListener(RedisMessenger listener);

    void subscribePubSub(String... channels);

    boolean isConnected();

    CompletableFuture<String> asyncGetStr(String key);

    CompletableFuture<Long> asyncHScanStr(String key, String filter, KeyValueScanHandler onKeyValueScan);

    CompletableFuture<String> asyncHGetStr(String key, String field);

    CompletableFuture<Long> asyncPublish(String channel, String message);


}
