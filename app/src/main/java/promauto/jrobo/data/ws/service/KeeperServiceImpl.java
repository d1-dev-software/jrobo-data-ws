package promauto.jrobo.data.ws.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import promauto.jrobo.data.ws.dataclient.RedisClient;
import promauto.jrobo.data.ws.properties.AppProperties;
import promauto.jrobo.data.ws.protocol.Protocol;
import promauto.jrobo.data.ws.repo.TagsRepository;
import promauto.jrobo.data.ws.model.TagTimeData;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

@Slf4j
@Service
public class KeeperServiceImpl implements KeeperService {

    private final RedisClient client;
    private final TagsRepository repo;
    private final Protocol protocol;
    private final int cyclePeriodMs;
    private final String tagsFilter;
    private final int chunkCount;

    private volatile boolean connected = false;
    private volatile boolean needUpdateAll = false;
    private volatile long serverTime = 0;
    private final AtomicLong updateLockTime = new AtomicLong(0L);
    private int chunkIndex;


    @Autowired
    public KeeperServiceImpl(AppProperties properties, RedisClient client, TagsRepository repo, Protocol protocol) {
        this.client = client;
        this.repo = repo;
        this.protocol = protocol;

        cyclePeriodMs = properties.keeperCyclePeriodMs;
        tagsFilter = properties.tagsFilter;
        chunkCount = Math.max(properties.keeperChunkUpdatePeriodS * 1000 / cyclePeriodMs, 1);
    }

    @Override
    @Async
    public void start() {
        client.start();

        log.info("Keeper started");
        for(;;) {
            try {
                doCyclePass();
                Thread.sleep(cyclePeriodMs);
            } catch (InterruptedException e) {
                break;
            } catch (Exception e) {
                log.error("Unhandled exception in Keeper", e);
            }
        }
        log.info("Keeper finished");
    }


    private void doCyclePass() {
        if( !connected  &&  client.isConnected() ) {
            log.info("Keeper connected to redis");
            connected = true;
            needUpdateAll = true;

        } else if( connected  &&  !client.isConnected() ) {
            log.info("Keeper disconnected from redis");
            connected = false;

        } else if( client.isConnected() ) {
            if( needUpdateAll )
                updateAll();
            else
                updateChunk();

            updateServerTime();
        }
    }


    @Override
    public synchronized void updateTag(String tagname, long time, String data) {
        TagTimeData val = repo.getValue(tagname);
        if( val != null ) {
           if( val.time == time  &&  val.data.equals(data) )
               return;
           if( val.time > time  &&  val.time < (serverTime + cyclePeriodMs * 3) )
               return;
        }
        repo.put(tagname, new TagTimeData(time, data));
    }


    @Override
    public synchronized void deleteTag(String tagname, long time) {
        TagTimeData val = repo.getValue(tagname);
        if( val != null  &&  (val.time <= time  ||  val.time > (serverTime + cyclePeriodMs * 3)) ) {
            repo.remove(tagname);
        }
    }


    private void updateAll() {
        if( !updateLockTime.compareAndSet(0L, System.currentTimeMillis()) )
            return;

        log.info("Keeper is updating ...");
        needUpdateAll = false;
        final Set<String> tagnames = new HashSet<>();

        RedisClient.KeyValueScanHandler onScan = (tagname, data) -> {
            long time = protocol.decodeTimeFromData(data);
            updateTag(tagname, time, data);
            tagnames.add(tagname);
        };

        final long localUpdateLockTime = updateLockTime.get();
        client.asyncHScanStr(protocol.getTagsKey(), tagsFilter, onScan)
                .thenAccept(count -> {
                    if( updateLockTime.compareAndSet(localUpdateLockTime, 0L) ) {
                        repo.retainAll(tagnames);
                        log.info("Keeper updated successfully");
                    } else {
                        log.error("Keeper failed to update!");
                    }
                });
    }


    private void updateChunk() {
        if( updateLockTime.get() > 0 )
            return;

        int chunkSize = Math.max(repo.size() / chunkCount, 1);
        List<String> tagnames = repo.getKeySublist(chunkIndex, chunkSize);

        if( (chunkIndex += chunkSize) >= repo.size() )
            chunkIndex = 0;

        for(String tagname: tagnames) {
            client.asyncHGetStr(protocol.getTagsKey(), tagname)
                    .thenAccept(data -> {
                        if( data == null) {
                            deleteTag(tagname, serverTime);
                        } else {
                            updateTag(tagname, protocol.decodeTimeFromData(data), data);
                        }
                    });
        }
    }


    private void updateServerTime() {
        client.asyncGetStr(protocol.getServerTimeKey())
                .thenAccept(serverTimeStr -> {
                    try {
                        serverTime = Long.parseLong(serverTimeStr);
                    } catch (NumberFormatException e) {
                        serverTime = 0;
                    }
                });
    }


    @Override
    public Boolean isConnected() {
        return connected;
    }


    @Override
    public long getServerTime() {
        return serverTime;
    }
}
