package promauto.jrobo.data.ws.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import promauto.jrobo.data.ws.api.GroupsApiDelegate;
import promauto.jrobo.data.ws.model.*;
import promauto.jrobo.data.ws.service.GroupsService;

@Service
public class GroupsApiDelegateImpl implements GroupsApiDelegate {
    private final GroupsService groupsService;

    public GroupsApiDelegateImpl(GroupsService groupsService) {
        this.groupsService = groupsService;
    }

    @Override
    public ResponseEntity<GroupCreateResponseDto> createGroup(GroupCreateRequestDto groupCreateRequestDto) {
        Group group = groupsService.createGroup(groupCreateRequestDto.getTagNames());
        return new ResponseEntity<>(
                new GroupCreateResponseDto()
                        .groupId(group.id.toString()),
                HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Void> deleteOneGroup(String groupId) {
        return new ResponseEntity<>(
                groupsService.removeGroup(groupId)?
                        HttpStatus.OK:
                        HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity<GroupResponseDto> findOneGroup(String groupId) {
        return groupsService.getGroup(groupId)
                .map(group -> new ResponseEntity<>(
                        new GroupResponseDto()
                                .groupId(group.id)
                                .tagNames(group.tagnames),
                        HttpStatus.OK
                )).orElse(new ResponseEntity<>(
                        HttpStatus.NOT_FOUND
                ));
    }

    @Override
    public ResponseEntity<GroupDataResponseDto> retrieveGroupData(String groupId) {
        return groupsService.getGroupData(groupId)
                .map(data -> new ResponseEntity<>(
                        new GroupDataResponseDto()
                                .groupId(groupId)
                                .data(data),
                        HttpStatus.OK
                )).orElse(new ResponseEntity<>(
                        HttpStatus.NOT_FOUND
                ));
    }
}




/* todo: remove
{
"tagNames": [
     "NRMOL.GNR_060_1_Output",
     "NRMOL.GNR_Beep1_Output",
     "NRMSM.GNR_Beep1_Output",
     "SCHML.GNR_Beep1_Output",
     "SHDK.GNR_Beep1_Output",
     "SHDK.GNR_Beep2_Output",
     "SHDK.GNR_Vibro1_Output",
     "SHDK.GNR_Vibro2_Output",
     "SHDM.GNR_T02DD_Output",
     "SHDSH.GNR_Beep1_Output",
     "SHDZO.GNR_Beep1_Output",
     "VCHSU.GNR_Beep1_Output",
     "icp01.inp0.value",
     "icp01.inp1.value"
    ]
}
*/

