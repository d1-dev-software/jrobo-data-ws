package promauto.jrobo.data.ws.service;

import com.google.common.cache.CacheStats;
import promauto.jrobo.data.ws.model.Group;

import java.util.List;
import java.util.Optional;

public interface GroupsService {
    Group createGroup(List<String> tagNames);

    Optional<Group> getGroup(String groupId);

    boolean removeGroup(String groupId);

    Optional<List<String>> getGroupData(String groupId);

    List<String> getCacheContent();

    CacheStats getCacheStats();
}
