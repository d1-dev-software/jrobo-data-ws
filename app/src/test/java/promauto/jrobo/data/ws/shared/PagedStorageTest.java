package promauto.jrobo.data.ws.shared;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PagedStorageTest {
    static final long CACHE_MAXIMUM_SIZE = 10000;

    PagedStorage<String> repo;

    @BeforeEach
    void setUp() {
        repo = new PagedStorage<>(CACHE_MAXIMUM_SIZE, 60, true);
    }

    @Test
    void get_empty_page() {
        PagedStorage.Page page = repo.getPage("", 0, null);
        assertTrue(page.keys.isEmpty());
//        assertFalse(page.hasNextPage);
        assertTrue(page.nextPageStartKey.isEmpty());
    }

    @Test
    void get_page_without_filtering() {
        repo.put("aaa", "111");
        repo.put("bbb", "222");
        PagedStorage.Page page = repo.getPage("", 0, null);

        assertEquals(page.keys.size(), 2);
//        assertFalse(page.hasNextPage);
        assertTrue(page.nextPageStartKey.isEmpty());
    }

    @Test
    void get_page_with_filtering() {
        repo.put("aaa", "111");
        repo.put("bbb", "222");
        PagedStorage.Page page = repo.getPage("a.*", 0, null);

        assertEquals(page.keys.size(), 1);
        assertEquals(page.keys.get(0), "aaa");
//        assertFalse(page.hasNextPage);
        assertTrue(page.nextPageStartKey.isEmpty());
    }

    @Test
    void test_pages() {
        repo.put("a", "1");
        repo.put("b", "1");
        repo.put("c", "1");
        repo.put("aa", "2");
        repo.put("bb", "2");
        repo.put("cc", "2");
        repo.put("aaa", "3");
        repo.put("bbb", "3");
        repo.put("ccc", "3");

        PagedStorage.Page page = repo.getPage("a.*", 2, null);
        assertEquals(2,     page.keys.size()      );
        assertEquals("a",   page.keys.get(0)      );
        assertEquals("aa",  page.keys.get(1)      );
        assertEquals("aaa", page.nextPageStartKey );

        page = repo.getPage("a.*", 2, "aa");
        assertEquals(2,     page.keys.size()      );
        assertEquals("aa",  page.keys.get(0)      );
        assertEquals("aaa", page.keys.get(1)      );
        assertEquals("",    page.nextPageStartKey );
    }

    @Test
    void test_cache() {
        repo.put("a", "1");
        repo.put("aa", "2");
        repo.put("aaa", "3");
        repo.put("aaaa", "4");
        repo.put("aaaaa", "5");

        for(int i=0; i<100; ++i) {
            repo.getPage("", 0, null);
            repo.getPage("aa.*", 0, null);
            repo.getPage("aa.*", 2, null);
            repo.getPage("aa.*", 2, "aaa");
        }

        assertEquals( 4, repo.cache.size());

        assertEquals( 5, repo.cache.getIfPresent(".*").keys.size());
        assertEquals( 4, repo.cache.getIfPresent("aa.*").keys.size());
        assertEquals( 2, repo.cache.getIfPresent("aa.* #2").keys.size());
        assertEquals( 2, repo.cache.getIfPresent("aa.* #2 ^aaa").keys.size());

        for(int i = 0; i< CACHE_MAXIMUM_SIZE; ++i)
            repo.getPage(""+i, 0, null);

        assertThat(repo.cache.size(), lessThan(CACHE_MAXIMUM_SIZE));
    }

    @Test
    void test_cache_clear_on_update() {
        repo.put("a", "1");
        repo.getPage("1", 0, null);
        repo.getPage("2", 0, null);
        repo.getPage("3", 0, null);
        assertEquals( 3, repo.cache.size());

        repo.put("aa", "2");
        repo.getPage("4", 0, null);
        assertEquals( 1, repo.cache.size());

    }

    @Test
    void test_invalid_regex() {
        repo.put("a", "1");
        var page = repo.getPage("*", 0, null);
        assertEquals( 0, page.keys.size());
        assertEquals( "", page.nextPageStartKey);
        assertEquals( 0, repo.cache.size());

    }

}